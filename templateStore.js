define(function (require) {
      //Template engine
  var Haml = require('haml-js'),
      //List of templates to compile
      tmplOne = require('text!./tmpl/tmplOne.jshaml');
  
  return {
    //Compiled functions
    tmplOne: Haml( tmplOne , {customEscape: "Haml.html_escape"} )
  }
});