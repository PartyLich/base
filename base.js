require.config({
  paths: {  //Configure library/module paths.
//    'jquery' : 'https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min',
    'jquery' : 'lib/jquery-1.8.2.min',
    'tablesorter' : 'lib/jquery.tablesorter.min',
    'jsrender' : 'lib/jsrender'
  }
});


define(function(require) {
  var $ = require('jquery')/*,
//      jsrender = require("jsrender"),
//      tablesorter = require("tablesorter"),
//      Entry = require('js/entry')*/;

  require('jsrender');
  require('tablesorter');

  //Document ready.
  $(function () {

  });


});