/**
 *
 *
 * Inspired by jquery.dlmenu.js
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 * Copyright 2013, Codrops http://www.codrops.com
 */
define(function (require) {
  var $ = require('jquery');
//  'use strict';

  // global


  /**
   *
   */
  function Menu(element, options) {
//    var wrapperclass = 'dl-menuwrapper';

    this.$el = $( element );
    this.el = element[0];

    var prefix = (/mozilla/.test(navigator.userAgent.toLowerCase()) &&
          !/webkit/.test(navigator.userAgent.toLowerCase())) ? 'Moz' :
          (/webkit/.test(navigator.userAgent.toLowerCase())) ? 'Webkit' :
          (/msie/.test(navigator.userAgent.toLowerCase()))   ? 'ms' :
          (/opera/.test(navigator.userAgent.toLowerCase()))  ? 'O' : '',
        animEndEventNames = {
          'WebkitAnimation' : 'webkitAnimationEnd',
          'OAnimation' : 'oAnimationEnd',
          'msAnimation' : 'MSAnimationEnd',
          'animation' : 'animationend'
        },
        transEndEventNames = {
          'WebkitTransition' : 'webkitTransitionEnd',
          'MozTransition' : 'transitionend',
          'OTransition' : 'oTransitionEnd',
          'msTransition' : 'MSTransitionEnd',
          'transition' : 'transitionend'
        };
    // animation end event name
    this.animEndEventName = animEndEventNames[prefix + 'Animation'];
    // transition end event name
    this.transEndEventName = transEndEventNames[prefix + 'Transition'],


    this._init( options );

    return this;
  };

  // the options
  Menu.defaults = {
      // classes for the animation effects
      animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' },
      // callback: click a link that has a sub menu
      // el is the link element (li); name is the level name
      onLevelClick : function( el, name ) { return false; },
      // callback: click a link that does not have a sub menu
      // el is the link element (li); ev is the event obj
      onLinkClick : function( el, ev ) { return false; },
      backLabel: 'Back',
      useActiveItemAsBackLabel: false/*,
      open: false*/
  };


  Menu.prototype = {
    _init : function( options ) {
      // options
//      this.options = $.extend( true, {}, $.DLMenu.defaults, options );
      this.options = {};
      var key = null;

      options = (options || Menu.defaults);

      for(key in Menu.defaults) {
        if(typeof options[key] == "undefined") { options[key] = Menu.defaults[key]; }
      }

      // options[] has all the data - user provided and optional.
      for(key in options) {
        //console.log(key + " = " + options[key]);
        this.options[key] = options[key];
      }


      // Cache some elements and initialize some variables.
      this._config();

      this._initEvents();
    },


    /**
     * Cache some elements and initialize some variables.
     */
    _config : function() {
      this.open = false;

      this.$trigger = this.$el.closest( '.dl-trigger' );
//      console.log(this.el);
//      console.log(this.el.parentNode);
//      console.log(this.el.classList.contains('dl-trigger'));

      this.$menu = this.$el.children( 'menu.dl-menu' );
      this.$menuitems = this.$menu.find( 'span:not(.dl-back)' ).not('.skip');

      this.$menu.find( 'span.skip').on('click', function(e) {return false;});

      this.$el.find( 'menu.dl-submenu' ).prepend( '<span class="dl-back"><a href="#">' + this.options.backLabel + '</a></span>' );
      this.$back = this.$menu.find( 'span.dl-back' );

      //TODO: feature detection, cause chumps will be chumps.
      this.supportAnimations = true;

      //
      if(this.options.useActiveItemAsBackLabel) {
        this.$back.each(function () {
          var $this = $(this),
            parentLabel = $this.parents('span:first').find('a:first').text();

          $this.find('a').html(parentLabel);
        });
      }
    },

    _initEvents : function() {
      var self = this;

      //Toggle the root menu open-state when its trigger is clicked.
      this.$trigger.on('click', function (e) {
        console.log('click $el', this);
        //Trigger body click event to close other open menu(s).
        $('body').trigger('click.menu');

        if(self.open) {
          self.closeMenu();
        } else {
          console.log('opening...');
          self.openMenu();
        }

        return false;
      });



      /**
       * Menu item click handler.
       */
      this.$menuitems.on( 'click', function menu_click(e) {
        e.stopPropagation();

        var $item = $(this),
            $submenu = $item.children( 'menu.dl-submenu' );

        //If this item has a submenu...
        if($submenu.length) {
          self.openSubMenu($submenu, self);
        } else {
          //Call the link click handler.
          self.options.onLinkClick( $item, event );
        }
      });


      /**
       * Menu back item click handler.
       */
      this.$back.on( 'click', function ( event ) {
        var $this = $( this ),  //back button
            $submenu = $this.parents( 'menu.dl-submenu:first' ),  //submenu containing the back button
            $item = $submenu.parent(),

            $flyin = $submenu.clone().insertAfter( self.$menu );

        var onAnimationEndFn = function () {
          self.$menu.removeClass( self.options.animationClasses.classin );
          $flyin.remove();
        };

        setTimeout(function () {
          $flyin.addClass( self.options.animationClasses.classout );
          self.$menu.addClass( self.options.animationClasses.classin );

          if( self.supportAnimations ) {
            self.$menu.one( self.animEndEventName, onAnimationEndFn );
          }
          else {
            onAnimationEndFn.call();
          }

          $item.removeClass( 'dl-subviewopen' );

          var $subview = $this.parents( '.dl-subview:first' );
          if( $subview.is( 'span' ) ) {
            $subview.addClass( 'dl-subviewopen' );
          }
          $subview.removeClass( 'dl-subview' );
        });

        return false;
      });

    },


    /**
     * Close the entire menu.
     */
    closeMenu : function () {
      if( this.open ) {
        var self = this;

        this.$menu.removeClass( 'dl-menuopen' );
//        this.$menu.addClass( 'dl-menu-toggle' );
        this.$trigger.removeClass( 'dl-active' );

        if( this.supportTransitions ) {
          this.$menu.one( this.transEndEventName, function onTransitionEndFn() {
            self.resetMenu();
          });
        } else {
          self.resetMenu();
        }

        this.open = false;
      }
    },


    openMenu : function () {
      if( !this.open ) {
        var self = this;

        // clicking somewhere else makes the menu close
        $('body').one( 'click.menu', function(e) {
          self.closeMenu() ;
        });

        this.$menu.addClass( 'dl-menuopen dl-menu-toggle' )/*.on( this.transEndEventName, function() {
          $( this ).removeClass( 'dl-menu-toggle' );
        })*/;
        this.$trigger.addClass( 'dl-active' );
        this.open = true;
      }
    },


    /**
     * Open a submenu.
     * @param {jQuery} $submenu The submenu to open.
     */
    openSubMenu : function ($submenu, self) {
      var $flyin = $submenu.clone().css( {opacity : 0} ).insertAfter( self.$menu ),
          $item = $submenu.parent(),

          onAnimationEndFn = function() {
            self.$menu.removeClass( self.options.animationClasses.classout ).addClass( 'dl-subview' );
            $item.addClass( 'dl-subviewopen' ).parents( '.dl-subviewopen:first' ).removeClass( 'dl-subviewopen' ).addClass( 'dl-subview' );
            $flyin.remove();
          };

      setTimeout(function () {
        $flyin.addClass( self.options.animationClasses.classin );
        console.log($flyin);
        self.$menu.addClass( self.options.animationClasses.classout );

        if( self.supportAnimations ) {
          self.$menu.one( self.animEndEventName, onAnimationEndFn );
        }
        else {
          onAnimationEndFn.call();
        }

        self.options.onLevelClick( $item, $item.children( 'a:first' ).text() );
      });

      return false;
    },


    /**
     * Resets the menu to its original state (first level of options)
     */
    resetMenu : function() {
      this.$menu.removeClass( 'dl-subview' );
      this.$menuitems.removeClass( 'dl-subview dl-subviewopen' );
    }
  };

  var logError = function( message ) {
    if( window.console ) {
      window.console.error( message );
    }
  };


  return Menu;
});