({
  /*appDir: ".",*/

  //By default all the configuration for optimization happens from the command
  //line or by properties in the config file, and configuration that was
  //passed to requirejs as part of the app's runtime "main" JS file is *not*
  //considered. However, if you prefer the "main" JS file configuration
  //to be read for the build so that you do not have to duplicate the values
  //in a separate configuration, set this property to the location of that
  //main JS file. The first requirejs({}), require({}), requirejs.config({}),
  //or require.config({}) call found in that file will be used.
//  mainConfigFile: 'parse.js',

  baseUrl: ".",
  dir: "../base-build",
  optimize: "closure",
  generateSourceMaps: !0,
  modules: [{name:"miTracker"}],
  paths: {
    /*jquery:"empty:",*/
    jquery: "lib/jquery-1.8.2.min",
    'tablesorter' : 'lib/jquery.tablesorter.min',
    'jsrender' : 'lib/jsrender'
  },

  //If using Closure Compiler for script optimization, these config options
  //can be used to configure Closure Compiler. See the documentation for
  //Closure compiler for more information.
  closure: {
//    CompilerOptions: {},
//    CompilationLevel: 'SIMPLE_OPTIMIZATIONS',
    loggingLevel: 'WARNING'
  },

  //Allow CSS optimizations. Allowed values:
  //- "standard": @import inlining, comment removal and line returns.
  //Removing line returns may have problems in IE, depending on the type
  //of CSS.
  //- "standard.keepLines": like "standard" but keeps line returns.
  //- "none": skip CSS optimizations.
  //- "standard.keepComments": keeps the file comments, but removes line
  //returns.  (r.js 1.0.8+)
  //- "standard.keepComments.keepLines": keeps the file comments and line
  //returns. (r.js 1.0.8+)
  optimizeCss: "standard"
})